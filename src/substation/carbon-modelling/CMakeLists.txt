set(CARBON_MODELLING_PUBLIC_HEADERS
    abstractcarbonintensityinterface.h
    abstractcarbonintensitystorage.h
    carbonintensity.h
    carbonintensitycurve.h
    carbonintensitymonitor.h
    electricitymapscarbonintensityinterface.h
    filebasedcarbonintensitystorage.h
    inmemorycarbonintensitystorage.h
)
add_library(carbon-modelling
    ${CARBON_MODELLING_PUBLIC_HEADERS}
    carbonintensity.cpp
    carbonintensitycurve.cpp
    carbonintensitymonitor.cpp
    electricitymapscarbonintensityinterface.cpp
    filebasedcarbonintensitystorage.cpp
    inmemorycarbonintensitystorage.cpp
)
target_include_directories(carbon-modelling
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    $<INSTALL_INTERFACE:include>
)
target_link_libraries(carbon-modelling PUBLIC common cpr::cpr nlohmann_json::nlohmann_json spdlog::spdlog)
set_target_properties(carbon-modelling PROPERTIES 
    PREFIX ${LIBSUBSTATION_PREFIX}
    PUBLIC_HEADER "${CARBON_MODELLING_PUBLIC_HEADERS}"
)

install(TARGETS carbon-modelling
    EXPORT carbon-modelling-targets
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin
    PUBLIC_HEADER DESTINATION include/substation/carbon-modelling
)

# Export the targets for other projects
install(EXPORT carbon-modelling-targets
    FILE carbon-modelling-config.cmake
    NAMESPACE substation::
    DESTINATION lib/cmake/substation
)

if (BUILD_TESTS)
    add_subdirectory(tests)
endif()