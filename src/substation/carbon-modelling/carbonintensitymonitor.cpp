//
// Created by Claudio Cambra on 13/2/25.
//

#include "carbonintensitymonitor.h"

#include <spdlog/spdlog.h>

#include <substation/common/event.h>
#include <substation/common/eventloop.h>

namespace Substation::CarbonModelling
{

CarbonIntensityMonitor::CarbonIntensityMonitor(const std::shared_ptr<Geolocation::AbstractGeolocationInterface> &gl_interface,
                                               const std::shared_ptr<AbstractCarbonIntensityInterface> &ci_interface,
                                               const std::shared_ptr<AbstractCarbonIntensityStorage> &ci_storage,
                                               const Callbacks &callbacks,
                                               const std::chrono::seconds interval)
    : m_gl_interface(gl_interface)
    , m_ci_interface(ci_interface)
    , m_ci_storage(ci_storage)
    , m_interval(interval)
{
    add_callbacks(callbacks);
    if (m_gl_interface == nullptr) {
        throw std::invalid_argument("Geolocation interface cannot be null");
    }
    if (m_ci_interface == nullptr) {
        throw std::invalid_argument("Carbon intensity interface cannot be null");
    }
    if (m_ci_storage) {
        if (const auto latest_intensity = m_ci_storage->latest_intensity()) {
            m_last_check.store(std::chrono::system_clock::from_time_t(latest_intensity->datetime));
        }
    }
}

std::shared_ptr<CarbonIntensityMonitor>
CarbonIntensityMonitor::create(const std::shared_ptr<Substation::Geolocation::AbstractGeolocationInterface> &gl_interface,
                               const std::shared_ptr<AbstractCarbonIntensityInterface> &ci_interface,
                               const std::shared_ptr<AbstractCarbonIntensityStorage> &ci_storage,
                               const Callbacks &callbacks,
                               const std::chrono::seconds interval)
{
    return std::shared_ptr<CarbonIntensityMonitor>(new CarbonIntensityMonitor(gl_interface, ci_interface, ci_storage, callbacks, interval));
}

// Do not call this within the constructor. We need to get the shared pointer to this object
// before we can call start, and this won't work prior to full construction.
void CarbonIntensityMonitor::start()
{
    if (m_running.load()) {
        return;
    }
    m_running.store(true);
    Common::EventLoop::get_instance()->execute([this] {
        monitor();
    });
}

void CarbonIntensityMonitor::stop()
{
    m_running.store(false);
}

void CarbonIntensityMonitor::fetch()
{
    if (!m_running.load()) {
        spdlog::debug("Monitor not running, will not run fetch.");
        return;
    }

    const auto weak_this = weak_from_this();
    Common::EventLoop::get_instance()->execute([weak_this] {
        if (const auto strong_this = weak_this.lock()) {
            if (strong_this->m_running.load()) {
                strong_this->fetch_carbon_intensity();
            }
        } else if (weak_this.expired()) {
            spdlog::debug("Monitor expired, will not run fetch.");
        }
    });
}

void CarbonIntensityMonitor::monitor()
{
    if (!m_running.load()) {
        spdlog::debug("Monitor not running, will not run monitoring.");
        return;
    }

    const auto weak_this = weak_from_this();
    const auto monitor_callback = [weak_this] {
        if (const auto strong_this = weak_this.lock()) {
            if (strong_this->m_running.load()) {
                strong_this->monitor();
            }
        } else if (weak_this.expired()) {
            spdlog::debug("Monitor expired, will not run monitoring.");
        }
    };

    const auto now = std::chrono::system_clock::now();
    if (now < m_last_check.load() + m_interval.load()) {
        spdlog::info("Rerunning monitoring in: {} seconds", (m_last_check.load() + m_interval.load() - now).count());
        Common::EventLoop::get_instance()->add_event({m_last_check.load() + m_interval.load(), monitor_callback});
        return;
    }

    spdlog::info("Checking carbon intensity now: {}", now.time_since_epoch().count());
    if (!fetch_carbon_intensity().has_value()) {
        spdlog::info("Retrying carbon intensity fetch in: {} seconds", INTERNAL_CHECK_INTERVAL.count());
        Common::EventLoop::get_instance()->add_event({now + INTERNAL_CHECK_INTERVAL, monitor_callback});
        return;
    }

    m_last_check = now;
    spdlog::info("Successfully acquired carbon intensity at {}", m_last_check.load().time_since_epoch().count());
    spdlog::info("Next check in: {} seconds", m_interval.load().count());
    Common::EventLoop::get_instance()->add_event({now + m_interval.load(), monitor_callback});
}

std::expected<CarbonIntensity, CarbonIntensityMonitor::Error> CarbonIntensityMonitor::fetch_carbon_intensity()
{
    std::scoped_lock lock(m_callbacks_mutex);

    spdlog::info("Starting geolocation fetch for carbon intensity.");
    for (const auto &callback : m_geolocation_fetch_started_callbacks) {
        callback();
    }
    const auto geolocation = m_gl_interface->get_geolocation().get();
    if (!geolocation.has_value()) {
        spdlog::error("Geolocation fetch failed, cannot acquire local carbon intensity.");
        return std::unexpected(Error::geolocation_fetch_error);
    }
    spdlog::info("Geolocation: {} ({}, {})", geolocation->name, geolocation->latitude, geolocation->longitude);
    for (const auto &callback : m_geolocation_fetched_callbacks) {
        callback(*geolocation);
    }

    spdlog::info("Starting carbon intensity fetch.");
    for (const auto &callback : m_intensity_fetch_started_callbacks) {
        callback();
    }
    const auto carbon_intensity = m_ci_interface->get_carbon_intensity(*geolocation).get();
    if (!carbon_intensity.has_value()) {
        spdlog::error("Carbon intensity fetch failed.");
        return std::unexpected(Error::carbon_intensity_fetch_error);
    }
    spdlog::info("Carbon Intensity: {} (at {})", carbon_intensity->intensity, carbon_intensity->datetime);
    if (m_intensity_fetched_callbacks.empty() && m_ci_storage) {
        if (const auto store_error = m_ci_storage->store(*carbon_intensity)) {
            spdlog::error("Failed to store carbon intensity: {}", store_error->message);
            return std::unexpected(Error::store_error);
        }
    } else {
        for (const auto &callback : m_intensity_fetched_callbacks) {
            callback(*carbon_intensity);
        }
    }
    return *carbon_intensity;
}

}