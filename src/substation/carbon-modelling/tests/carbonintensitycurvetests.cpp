//
// Created by Claudio Cambra on 06/02/25.
//

#include <catch2/catch_all.hpp>
#include <chrono>

#include <substation/carbon-modelling/carbonintensity.h>
#include <substation/carbon-modelling/carbonintensitycurve.h>

using namespace Substation::CarbonModelling;

namespace
{
constexpr std::time_t TIME_T_HOUR = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::hours(1)).count();
constexpr auto TIME_T_24_HRS = TIME_T_HOUR * 24;
constexpr auto TIME_T_12_HRS = TIME_T_HOUR * 24;
constexpr auto TIME_T_3_HRS = TIME_T_HOUR * 3;
}

static inline CarbonIntensity generate_default_intensity(const int intensity, const std::time_t datetime)
{
    const auto now = static_cast<int>(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
    return {.zone = "ES",
            .intensity = intensity,
            .datetime = datetime,
            .updated_at = now,
            .emission_factor_type = "unknown",
            .is_estimated = false,
            .estimation_method = "unknown"};
}

TEST_CASE("Carbon intensity curve returns added values", "[carbon-intensity-curve-real-values]")
{
    const auto ci_1 = ::generate_default_intensity(100, 1640601600);
    const auto ci_2 = ::generate_default_intensity(110, 1640601610);
    auto curve = CarbonIntensityCurve({ci_1, ci_2});
    const auto returned_ci_1 = curve.intensity_at(ci_1.datetime);
    const auto returned_ci_2 = curve.intensity_at(ci_2.datetime);
    REQUIRE(returned_ci_1.has_value());
    REQUIRE(returned_ci_2.has_value());
    REQUIRE(returned_ci_1.value() == ci_1);
    REQUIRE(returned_ci_2.value() == ci_2);
}

TEST_CASE("Carbon intensity curve returns synthetic values", "[carbon-intensity-curve-synthetic-values]")
{
    const auto ci_1 = ::generate_default_intensity(100, 1640601600);
    const auto ci_2 = ::generate_default_intensity(110, 1640601610);
    const auto center_intensity = (ci_2.intensity + ci_1.intensity) / 2;
    auto curve = CarbonIntensityCurve({ci_1, ci_2});
    const auto synthetic_ci_opt = curve.intensity_at(1640601605);
    REQUIRE(synthetic_ci_opt.has_value());
    const auto &synthetic_ci = synthetic_ci_opt.value();
    REQUIRE(synthetic_ci.is_synthetic);
    REQUIRE(synthetic_ci.intensity == center_intensity);
}

TEST_CASE("Carbon intensity curve accurately provides lowest intensity period", "[carbon-intensity-curve-lowest-period]")
{
    /**
     * A small graph of what our test values look like.
     * The result should find the lowest overall intensity period for a given duration.
     * So a duration of:
     + - 30 -> period between 730 and 760
     * - 50 -> period between 710 and 760
     * - 100 -> period between 660 and 760
     *
     *  INTENSITY VALUE
     *  140 |                 *                                               *   *   *
     *  130 |             *       *                                                       *   *   *
     *  120 |         *               *   *           *   *   *           *
     *  110 |     *                           *   *
     *  100 | *                                                   *   *
     *  90  |---------------------------------------------------------------------------------------
     *  TIME 610 620 630 640 650 660 670 680 690 700 710 720 730 740 750 760 770 780 790 800 810 820
     */

    const auto ci_1 = ::generate_default_intensity(100, 610);
    const auto ci_2 = ::generate_default_intensity(110, 620);
    const auto ci_3 = ::generate_default_intensity(120, 630);
    const auto ci_4 = ::generate_default_intensity(130, 640);
    const auto ci_5 = ::generate_default_intensity(140, 650);
    const auto ci_6 = ::generate_default_intensity(130, 660);
    const auto ci_7 = ::generate_default_intensity(120, 670);
    const auto ci_8 = ::generate_default_intensity(120, 680);
    const auto ci_9 = ::generate_default_intensity(110, 690);
    const auto ci_10 = ::generate_default_intensity(110, 700);
    const auto ci_11 = ::generate_default_intensity(120, 710);
    const auto ci_12 = ::generate_default_intensity(120, 720);
    const auto ci_13 = ::generate_default_intensity(120, 730);
    const auto ci_14 = ::generate_default_intensity(100, 740);
    const auto ci_15 = ::generate_default_intensity(100, 750);
    const auto ci_16 = ::generate_default_intensity(120, 760);
    const auto ci_17 = ::generate_default_intensity(140, 770);
    const auto ci_18 = ::generate_default_intensity(140, 780);
    const auto ci_19 = ::generate_default_intensity(140, 790);
    const auto ci_20 = ::generate_default_intensity(130, 800);
    const auto ci_21 = ::generate_default_intensity(130, 810);
    const auto ci_22 = ::generate_default_intensity(130, 820);

    auto curve = CarbonIntensityCurve(
        {ci_1, ci_2, ci_3, ci_4, ci_5, ci_6, ci_7, ci_8, ci_9, ci_10, ci_11, ci_12, ci_13, ci_14, ci_15, ci_16, ci_17, ci_18, ci_19, ci_20, ci_21, ci_22});

    const auto check_period = [&curve](const int duration, const int expected_start, const int expected_end) {
        const auto lowest_period = curve.lowest_intensity_period(duration);
        REQUIRE(lowest_period.has_value());
        const auto &period_intensities = lowest_period.value().intensities;
        REQUIRE(period_intensities.front().datetime == expected_start);
        REQUIRE(period_intensities.back().datetime == expected_end);
    };

    SECTION("Duration 30 test")
    {
        check_period(30, 730, 760);
    }
    SECTION("Duration 50 test")
    {
        check_period(50, 710, 760);
    }
    SECTION("Duration 100 test")
    {
        check_period(100, 660, 760);
    }
}

TEST_CASE("Carbon intensity curve provides predicted values based on prior 24hrs", "[carbon-intensity-curve-predicted-values-24h]")
{
    const auto ci_1 = ::generate_default_intensity(100, 1640601600);
    // 6 hours after ci_1
    const auto ci_2 = ::generate_default_intensity(110, ci_1.datetime + (60 * 360));
    const auto center_intensity = (ci_2.intensity + ci_1.intensity) / 2;

    const auto require_predicted_ci =
        [&ci_1, &ci_2](const CarbonIntensityCurve::PredictionMethodFlags predicted_method, const std::time_t &at_datetime, const int expected_intensity) {
            auto curve = CarbonIntensityCurve({ci_1, ci_2});
            const auto predicted_ci_opt = curve.intensity_at(at_datetime, predicted_method);
            REQUIRE(predicted_ci_opt.has_value());
            const auto &predicted_ci = *predicted_ci_opt;
            REQUIRE(predicted_ci.is_synthetic);
            REQUIRE(predicted_ci.is_estimated);
            REQUIRE(predicted_ci.is_forecast);
            REQUIRE(predicted_ci.intensity == expected_intensity);
        };

    SECTION("TEST WITH USE_PAST_24H METHOD")
    {
        require_predicted_ci(CarbonIntensityCurve::use_past_24h, ci_1.datetime + TIME_T_24_HRS, ci_1.intensity);
    }

    SECTION("TEST WITH LOOSE_USE_PAST_24H METHOD")
    {
        require_predicted_ci(CarbonIntensityCurve::loose_use_past_24h, ci_1.datetime + TIME_T_24_HRS, ci_1.intensity);
    }

    SECTION("TEST WITH ANY_USE_PAST_24H METHOD")
    {
        require_predicted_ci(CarbonIntensityCurve::any_use_past_24h, ci_1.datetime + TIME_T_24_HRS, ci_1.intensity);
    }

    // Below: 24 + 3 hours after ci_1, so, a day ago in between ci_1 and ci_2 (requires interpolation)
    SECTION("TEST WITH USE_PAST_24H METHOD WITH INTERPOLATION")
    {
        require_predicted_ci(CarbonIntensityCurve::use_past_24h, ci_1.datetime + TIME_T_24_HRS + TIME_T_3_HRS, center_intensity);
    }

    SECTION("TEST WITH LOOSE_USE_PAST_24H METHOD WITH INTERPOLATION")
    {
        require_predicted_ci(CarbonIntensityCurve::loose_use_past_24h, ci_1.datetime + TIME_T_24_HRS + TIME_T_3_HRS, center_intensity);
    }

    SECTION("TEST WITH ANY_USE_PAST_24H METHOD WITH INTERPOLATION")
    {
        require_predicted_ci(CarbonIntensityCurve::any_use_past_24h, ci_1.datetime + TIME_T_24_HRS + TIME_T_3_HRS, center_intensity);
    }
}

TEST_CASE("Carbon intensity curve provides lowest carbon period with future values provided by past 24hr prediction",
          "[carbon-intensity-curve-lowest-period-predicted-values-24h]")
{
    // Generate a V-shaped curve that provides data every hour over 24 hours
    const auto ci_0 = ::generate_default_intensity(120, 0 * TIME_T_HOUR);
    const auto ci_1 = ::generate_default_intensity(110, 1 * TIME_T_HOUR);
    const auto ci_2 = ::generate_default_intensity(100, 2 * TIME_T_HOUR);
    const auto ci_3 = ::generate_default_intensity(90, 3 * TIME_T_HOUR);
    const auto ci_4 = ::generate_default_intensity(80, 4 * TIME_T_HOUR);
    const auto ci_5 = ::generate_default_intensity(70, 5 * TIME_T_HOUR);
    const auto ci_6 = ::generate_default_intensity(60, 6 * TIME_T_HOUR);
    const auto ci_7 = ::generate_default_intensity(50, 7 * TIME_T_HOUR);
    const auto ci_8 = ::generate_default_intensity(40, 8 * TIME_T_HOUR);
    const auto ci_9 = ::generate_default_intensity(30, 9 * TIME_T_HOUR);
    const auto ci_10 = ::generate_default_intensity(20, 10 * TIME_T_HOUR);
    const auto ci_11 = ::generate_default_intensity(10, 11 * TIME_T_HOUR);
    const auto ci_12 = ::generate_default_intensity(0, 12 * TIME_T_HOUR);
    const auto ci_13 = ::generate_default_intensity(10, 13 * TIME_T_HOUR);
    const auto ci_14 = ::generate_default_intensity(20, 14 * TIME_T_HOUR);
    const auto ci_15 = ::generate_default_intensity(30, 15 * TIME_T_HOUR);
    const auto ci_16 = ::generate_default_intensity(40, 16 * TIME_T_HOUR);
    const auto ci_17 = ::generate_default_intensity(50, 17 * TIME_T_HOUR);
    const auto ci_18 = ::generate_default_intensity(60, 18 * TIME_T_HOUR);
    const auto ci_19 = ::generate_default_intensity(70, 19 * TIME_T_HOUR);
    const auto ci_20 = ::generate_default_intensity(80, 20 * TIME_T_HOUR);
    const auto ci_21 = ::generate_default_intensity(90, 21 * TIME_T_HOUR);
    const auto ci_22 = ::generate_default_intensity(100, 22 * TIME_T_HOUR);
    const auto ci_23 = ::generate_default_intensity(110, 23 * TIME_T_HOUR);
    const auto ci_24 = ::generate_default_intensity(120, 24 * TIME_T_HOUR);

    /* CarbonIntensityCurve::lowest_intensity_period should generate the following intensities:
            intensity: 0 - 120
            intensity: 3600 - 110
            intensity: 7200 - 100
            intensity: 10800 - 90
            intensity: 14400 - 80
            intensity: 18000 - 70
            intensity: 21600 - 60
            intensity: 25200 - 50
            intensity: 28800 - 40
            intensity: 32400 - 30
            intensity: 36000 - 20
            intensity: 39600 - 10
            intensity: 43200 - 0
            intensity: 46800 - 10
            intensity: 50400 - 20
            intensity: 54000 - 30
            intensity: 57600 - 40
            intensity: 61200 - 50
            intensity: 64800 - 60
            intensity: 68400 - 70
            intensity: 72000 - 80
            intensity: 75600 - 90
            intensity: 79200 - 100
            intensity: 82800 - 110
            intensity: 86400 - 120
            Predicted intensity: 90000 - 110
            Predicted intensity: 93600 - 100
            Predicted intensity: 97200 - 90
            Predicted intensity: 100800 - 80
            Predicted intensity: 104400 - 70
    -> Lowest period start
            Predicted intensity: 108000 - 60
            Predicted intensity: 111600 - 50
            Predicted intensity: 115200 - 40
            Predicted intensity: 118800 - 30
            Predicted intensity: 122400 - 20
            Predicted intensity: 126000 - 10
            Predicted intensity: 129600 - 0
    -> Lowest period end
    */

    const std::vector<CarbonIntensity> intensities{ci_0,  ci_1,  ci_2,  ci_3,  ci_4,  ci_5,  ci_6,  ci_7,  ci_8,  ci_9,  ci_10, ci_11, ci_12,
                                                   ci_13, ci_14, ci_15, ci_16, ci_17, ci_18, ci_19, ci_20, ci_21, ci_22, ci_23, ci_24};
    const auto require_predicted_lowest_period = [&intensities](const CarbonIntensityCurve::PredictionMethodFlags predicted_method) {
        auto curve = CarbonIntensityCurve(intensities);
        const auto lowest_period = curve.lowest_intensity_period(18 * TIME_T_HOUR, 36 * TIME_T_HOUR, 6 * TIME_T_HOUR, predicted_method);
        REQUIRE(lowest_period.has_value());
        const auto &period_intensities = lowest_period->intensities;
        REQUIRE(period_intensities.front().datetime >= 18 * TIME_T_HOUR);
        REQUIRE(period_intensities.back().datetime <= 36 * TIME_T_HOUR);
        REQUIRE(period_intensities.back().datetime - period_intensities.front().datetime == 6 * TIME_T_HOUR);
        // Check for expected lowest period -- with our values, it should be at the very end
        REQUIRE(period_intensities.front().datetime == 30 * TIME_T_HOUR);
        REQUIRE(period_intensities.back().datetime == 36 * TIME_T_HOUR);
        REQUIRE(period_intensities.front().intensity == 60);
        REQUIRE(period_intensities.back().intensity == 0);
    };

    SECTION("TEST WITH USE_PAST_24H METHOD")
    {
        require_predicted_lowest_period(CarbonIntensityCurve::use_past_24h);
    }

    SECTION("TEST WITH LOOSE_USE_PAST_24H METHOD")
    {
        require_predicted_lowest_period(CarbonIntensityCurve::loose_use_past_24h);
    }

    SECTION("TEST WITH ANY_USE_PAST_24H METHOD")
    {
        require_predicted_lowest_period(CarbonIntensityCurve::any_use_past_24h);
    }
}

TEST_CASE("Carbon intensity curve 24hr prediction works with a range smaller than 24 hours", "[carbon-intensity-curve-24h-prediction-small-range]")
{
    const auto ci_0 = ::generate_default_intensity(120, 0 * TIME_T_HOUR);
    const auto ci_1 = ::generate_default_intensity(110, 1 * TIME_T_HOUR);
    const auto ci_2 = ::generate_default_intensity(100, 2 * TIME_T_HOUR);
    const auto ci_3 = ::generate_default_intensity(90, 3 * TIME_T_HOUR);
    const auto ci_4 = ::generate_default_intensity(80, 4 * TIME_T_HOUR);
    const auto ci_5 = ::generate_default_intensity(70, 5 * TIME_T_HOUR);
    const auto ci_6 = ::generate_default_intensity(60, 6 * TIME_T_HOUR); // 21600
    const auto ci_7 = ::generate_default_intensity(50, 7 * TIME_T_HOUR);
    const auto ci_8 = ::generate_default_intensity(40, 8 * TIME_T_HOUR);
    const auto ci_9 = ::generate_default_intensity(30, 9 * TIME_T_HOUR);
    const auto ci_10 = ::generate_default_intensity(20, 10 * TIME_T_HOUR);
    const auto ci_11 = ::generate_default_intensity(10, 11 * TIME_T_HOUR);
    const auto ci_12 = ::generate_default_intensity(0, 12 * TIME_T_HOUR); // 43200
    const std::vector<CarbonIntensity> intensities{ci_0, ci_1, ci_2, ci_3, ci_4, ci_5, ci_6, ci_7, ci_8, ci_9, ci_10, ci_11, ci_12};

    SECTION("TEST VALID INTENSITY PERIOD")
    {
        auto curve = CarbonIntensityCurve(intensities);
        const auto search_start = ci_6.datetime;
        const auto search_end = ci_6.datetime + ((24 + 6) * TIME_T_HOUR);
        const auto duration = 12 * TIME_T_HOUR;

        const auto lowest_period = curve.lowest_intensity_period(search_start, search_end, duration, CarbonIntensityCurve::loose_use_past_24h);
        REQUIRE(lowest_period.has_value());
        const auto &period_intensities = lowest_period->intensities;
        REQUIRE(period_intensities.front().datetime == 29 * TIME_T_HOUR);
        REQUIRE(period_intensities.back().datetime >= ci_6.datetime + TIME_T_24_HRS);
        REQUIRE(period_intensities.back().datetime - period_intensities.front().datetime >= 6 * TIME_T_HOUR);
        // Check for expected lowest period -- with our values, it should be at the very end
        REQUIRE(period_intensities.front().intensity == 70);
        REQUIRE(period_intensities.back().intensity == 0);
    }

    SECTION("TEST EXPECTED INTENSITY TIME JUMP DUE TO LACK OF 24H DATA")
    {
        auto curve = CarbonIntensityCurve(intensities);
        // Should cover all available data and provide some new predicted intensities
        const auto lowest_period =
            curve.lowest_intensity_period(ci_0.datetime, ci_12.datetime + TIME_T_12_HRS, TIME_T_24_HRS, CarbonIntensityCurve::loose_use_past_24h);
        REQUIRE(lowest_period.has_value());
        const auto &period_intensities = lowest_period->intensities;
        REQUIRE(period_intensities.size() < 24);
        REQUIRE(period_intensities.back().datetime - period_intensities.front().datetime >= TIME_T_24_HRS);
        auto found_intensity_gap = false;
        
        for (auto i = 1; i < period_intensities.size(); ++i) {
            const auto &prev = period_intensities[i - 1];
            const auto &curr = period_intensities[i];
            if (curr.datetime - prev.datetime > TIME_T_HOUR) {
                found_intensity_gap = true;
                break;
            }
        }

        REQUIRE(found_intensity_gap);
    }
}

