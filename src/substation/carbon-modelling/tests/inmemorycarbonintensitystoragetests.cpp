//
// Created by Claudio Cambra on 22/1/25.
//

#include <catch2/catch_all.hpp>

#include <substation/carbon-modelling/carbonintensity.h>
#include <substation/carbon-modelling/inmemorycarbonintensitystorage.h>

using namespace Substation::CarbonModelling;

TEST_CASE("In memory carbon intensity store correctly stores intensity", "[in-memory-ci-storage-store]")
{
    InMemoryCarbonIntensityStorage storage;
    auto callback_called = false;
    storage.register_store_callback([&callback_called](const CarbonIntensity &ci) {
        callback_called = true;
    });
    const auto ci = CarbonIntensity{.zone = "ES",
                                    .intensity = 100,
                                    .datetime = 1640601600,
                                    .updated_at = 1640601600,
                                    .emission_factor_type = "unknown",
                                    .is_estimated = false,
                                    .estimation_method = "unknown"};
    storage.store(ci);
    const auto data = storage.data();
    REQUIRE(data.has_value());
    REQUIRE(data->size() == 1);
    REQUIRE(data->front() == ci);
    REQUIRE(callback_called);
}

TEST_CASE("In memory carbon intensity store correctly retrieves stored intensity", "[in-memory-ci-storage-retrieve]")
{
    InMemoryCarbonIntensityStorage storage;
    const auto ci = CarbonIntensity{.zone = "ES",
                                    .intensity = 100,
                                    .datetime = 1640601600,
                                    .updated_at = 1640601600,
                                    .emission_factor_type = "unknown",
                                    .is_estimated = false,
                                    .estimation_method = "unknown"};
    const auto other_ci = CarbonIntensity{.zone = "ES",
                                          .intensity = 200,
                                          .datetime = 1640801650,
                                          .updated_at = 1640801650,
                                          .emission_factor_type = "unknown",
                                          .is_estimated = false,
                                          .estimation_method = "unknown"};
    storage.store(ci);
    storage.store(other_ci);
    const auto retrieved = storage.retrieve(ci.datetime);
    REQUIRE(retrieved.has_value());
    REQUIRE(retrieved.value() == ci);
}

// TEST ERRORS
// TEST AUTOMATIC CREATION OF STORE FOLDER