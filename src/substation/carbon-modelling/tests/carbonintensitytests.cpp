//
// Created by Claudio Cambra on 27/12/24.
//

#include <catch2/catch_all.hpp>
#include <filesystem>

#include <substation/carbon-modelling/carbonintensity.h>

using namespace Substation::CarbonModelling;

TEST_CASE("Writing carbon intensity to file works correctly", "[carbon-intensity-to-file]")
{
    const auto carbon_intensity = CarbonIntensity{.zone = "ES",
                                                  .intensity = 100,
                                                  .datetime = 1640601600,
                                                  .updated_at = 1640601600,
                                                  .emission_factor_type = "unknown",
                                                  .is_estimated = false,
                                                  .estimation_method = "unknown"};
    const auto path = std::filesystem::path("/tmp/carbon_intensity.json");
    REQUIRE(carbon_intensity.write_to_file(path));
    std::filesystem::remove(path);
}

TEST_CASE("Carbon intensity from file works correctly", "[carbon-intensity-from-file]")
{
    const auto carbon_intensity = CarbonIntensity{.zone = "ES",
                                                  .intensity = 200,
                                                  .datetime = 1640601600,
                                                  .updated_at = 1640601600,
                                                  .emission_factor_type = "unknown",
                                                  .is_estimated = false,
                                                  .estimation_method = "unknown"};
    const auto path = std::filesystem::path("/tmp/carbon_intensity.json");
    REQUIRE(carbon_intensity.write_to_file(path));
    const auto read_carbon_intensity = CarbonIntensity::from_file(path);
    std::filesystem::remove(path);
    REQUIRE(read_carbon_intensity != std::nullopt);
    REQUIRE(read_carbon_intensity->zone == carbon_intensity.zone);
    REQUIRE(read_carbon_intensity->intensity == carbon_intensity.intensity);
    REQUIRE(read_carbon_intensity->datetime == carbon_intensity.datetime);
    REQUIRE(read_carbon_intensity->updated_at == carbon_intensity.updated_at);
    REQUIRE(read_carbon_intensity->emission_factor_type == carbon_intensity.emission_factor_type);
    REQUIRE(read_carbon_intensity->is_estimated == carbon_intensity.is_estimated);
    REQUIRE(read_carbon_intensity->estimation_method == carbon_intensity.estimation_method);
}

TEST_CASE("Carbon intensity interpolation works as expected", "carbon-intensity-interpolation")
{
    const auto carbon_intensity_1 = CarbonIntensity{.zone = "ES",
                                                    .intensity = 100,
                                                    .datetime = 1640601600,
                                                    .updated_at = 1640601600,
                                                    .emission_factor_type = "unknown",
                                                    .is_estimated = false,
                                                    .estimation_method = "unknown"};

    const auto carbon_intensity_2 = CarbonIntensity{.zone = "ES",
                                                    .intensity = 200,
                                                    .datetime = 1640601610,
                                                    .updated_at = 1640601610,
                                                    .emission_factor_type = "unknown",
                                                    .is_estimated = false,
                                                   .estimation_method = "unknown"};

    const auto mid_interpolated_carbon_intensity = CarbonIntensity::interpolate(carbon_intensity_1, carbon_intensity_2, 1640601605);
    REQUIRE(mid_interpolated_carbon_intensity.intensity == 150);

    const auto close_to_start_interpolated_carbon_intensity = CarbonIntensity::interpolate(carbon_intensity_1, carbon_intensity_2, 1640601603);
    REQUIRE(close_to_start_interpolated_carbon_intensity.intensity == 130);

    const auto close_to_end_interpolated_carbon_intensity = CarbonIntensity::interpolate(carbon_intensity_1, carbon_intensity_2, 1640601608);
    REQUIRE(close_to_end_interpolated_carbon_intensity.intensity == 180);
}