//
// Created by Claudio Cambra on 27/12/24.
//

#include <catch2/catch_all.hpp>

#include <cstdlib>

#include <substation/carbon-modelling/electricitymapscarbonintensityinterface.h>
#include <substation/geolocation/geolocation.h>

using namespace Substation::CarbonModelling;

TEST_CASE("Carbon intensity fetching works correctly with electricity maps", "[em-ci]")
{
    const auto env_token = ::getenv("ELECTRICITY_MAPS_API_TOKEN");
    REQUIRE(env_token != nullptr);
    const auto electricity_maps_api_token = std::string(env_token);
    const auto geolocation = Substation::Geolocation::Geolocation{.name = "Madrid",
                                                                  .latitude = 40.4165f,
                                                                  .longitude = -3.70256f,
                                                                  .region = "M",
                                                                  .country = "ES",
                                                                  .timezone = "Europe/Madrid"};
    const auto carbon_intensity_interface = ElectricityMapCarbonIntensityInterface(electricity_maps_api_token);
    const auto carbon_intensity = carbon_intensity_interface.get_carbon_intensity(geolocation).get();
    REQUIRE(carbon_intensity != std::nullopt);
}