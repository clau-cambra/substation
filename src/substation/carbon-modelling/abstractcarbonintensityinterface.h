//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include <future>

#include "carbonintensity.h"
namespace Substation::Geolocation
{
struct Geolocation;
}

namespace Substation::CarbonModelling
{
class AbstractCarbonIntensityInterface
{
public:
    virtual ~AbstractCarbonIntensityInterface() = default;

    virtual std::future<std::optional<CarbonIntensity>> get_carbon_intensity(const Geolocation::Geolocation &location) const = 0;
};
}