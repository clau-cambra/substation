//
// Created by Claudio Cambra on 4/2/25.
//

#pragma once

#include <ctime>
#include <expected>
#include <optional>
#include <vector>

#include "abstractcarbonintensitystorage.h"
#include "carbonintensity.h"

namespace Substation::CarbonModelling
{

/**
 * The curve can provide real and synthetic carbon intensities.
 * The curve can provide intensities between the earliest and latest intensity.
 * It also provides some prediction methods to provide intensities beyond these earliest/latest values.
 *
 * The curve does not generate forecasts are future predictions.
 * It provide interpolated intensities where real or forecasted intensities are not available.
 */
class CarbonIntensityCurve
{
public:
    enum class error {
        no_error,
        no_data,
        request_too_far_in_past,
        request_too_far_in_future,
        request_invalid_duration
    };

    enum PredictionMethodFlags {
        none = 0,
        use_past_24h = 1 << 0, // Useful for future predictions
        loose_use_past_24h = 1 << 1, // Fill in as much as possible, may leave gaps
        any_use_past_24h = use_past_24h | loose_use_past_24h,
    };
    friend PredictionMethodFlags operator|(PredictionMethodFlags a, PredictionMethodFlags b)
    {
        return static_cast<PredictionMethodFlags>(static_cast<int>(a) | static_cast<int>(b));
    }

    enum IntensityTypeFlags {
        normal = 0,
        forecast = 1 << 0,
        synthetic = 1 << 1,
        all = normal | forecast | synthetic,
    };
    friend IntensityTypeFlags operator|(IntensityTypeFlags a, IntensityTypeFlags b)
    {
        return static_cast<IntensityTypeFlags>(static_cast<int>(a) | static_cast<int>(b));
    }

    struct Period {
        std::vector<CarbonIntensity> intensities;
        int min_intensity_index = -1;
        int max_intensity_index = -1;

        bool operator==(const Period &other) const = default;
    };

    explicit CarbonIntensityCurve(const std::vector<CarbonIntensity> &intensities = {}, const std::shared_ptr<AbstractCarbonIntensityStorage> &m_storage = {});

    /**
     * Adding a real (i.e. not forecasted) intensity that has the latest time will result in
     * recomputation of prior interpolated intensities between this and the preious non-forecasted
     * intensity.
     */
    void add_intensity(const CarbonIntensity &intensity);

    /**
     * Provide the number of intensities in the curve.
     */

    size_t intensity_count(IntensityTypeFlags types = all) const;

    /**
     * Finds the intensity at the given time.
     * If possible, will return a real or synthesised value within the curve start and end.
     * Otherwise, it will return a synthesised value outside of the curve range based on available
     * values and prediction methods requested.
     */
    std::optional<CarbonIntensity> intensity_at(std::time_t time, PredictionMethodFlags prediction_flags = none);

    /**
     * Finds the lowest overall intensity period between the search start and end times that lasts
     * for the given duration.
     */
    std::expected<Period, error>
    lowest_intensity_period(std::time_t search_start, std::time_t search_end, std::time_t duration, PredictionMethodFlags prediction_flags = none);

    /**
     * Finds the lowest overall intensity period between the search start and the latest intensity
     * time that lasts for the given duration.
     * The time between the earliest intensity and the search end time must be greater than or equal
     * to the duration.
     */
    std::expected<Period, error> lowest_intensity_period(std::time_t search_start, std::time_t duration, PredictionMethodFlags prediction_flags = none)
    {
        return lowest_intensity_period(search_start, m_intensities.back().datetime, duration, prediction_flags);
    }

    /**
     * Finds the lowest overall intensity period that lasts for the given duration.
     * The duration must be less than or equal to the time between the earliest and latest intensity.
     */
    std::expected<Period, error> lowest_intensity_period(std::time_t duration, PredictionMethodFlags prediction_flags = none)
    {
        return lowest_intensity_period(m_intensities.front().datetime, duration, prediction_flags);
    }

    void print_ascii_graph() const;

    CarbonIntensity operator[](const size_t index) const
    {
        return m_intensities[index];
    }

private:
    bool have_data_24_hours_prior(const std::time_t time) const;
    static std::optional<std::pair<CarbonIntensityCurve::error, std::string>>
    validate_search_parameters(std::time_t search_start, std::time_t search_end, std::time_t duration, const std::vector<CarbonIntensity> &search_vector);
    std::optional<CarbonIntensity> predicted_intensity(std::time_t time, PredictionMethodFlags prediction_flags);
    std::optional<std::vector<CarbonIntensity>>
    predict_intensities_for_search(std::time_t search_start, std::time_t search_end, PredictionMethodFlags prediction_flags);

    std::vector<CarbonIntensity> m_intensities;
    std::shared_ptr<AbstractCarbonIntensityStorage> m_storage;
};

}