//
// Created by Claudio Cambra on 19/2/25.
//

#pragma once

#include <filesystem>

#include "abstractcarbonintensitystorage.h"

namespace Substation::CarbonModelling
{

class FileBasedCarbonIntensityStorage : public AbstractCarbonIntensityStorage
{
public:
    static constexpr auto STORE_ENV_VAR_NAME = "CARBON_INTENSITY_STORE";

    explicit FileBasedCarbonIntensityStorage(const std::filesystem::path &store_folder = default_store_folder());
    ~FileBasedCarbonIntensityStorage() override = default;

    std::optional<StorageError> store(const CarbonIntensity &intensity) override;
    std::expected<std::vector<CarbonIntensity>, StorageError> data() const override;
    std::expected<CarbonIntensity, StorageError> retrieve(std::time_t datetime) const override;
    std::expected<CarbonIntensity, StorageError> earliest_intensity() const override;
    std::expected<CarbonIntensity, StorageError> latest_intensity() const override;

    std::filesystem::path store_folder_path() const
    {
        return m_store_folder_path;
    }

private:
    static std::filesystem::path default_store_folder();
    std::filesystem::path m_store_folder_path;
};

}