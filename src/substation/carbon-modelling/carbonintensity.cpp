//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#include "carbonintensity.h"

#include <fstream>
#include <iostream>

#include <spdlog/spdlog.h>

namespace Substation::CarbonModelling
{

std::optional<CarbonIntensity> CarbonIntensity::from_file(const std::filesystem::path &path) noexcept
{
    try {
        return nlohmann::json::parse(std::ifstream(path));
    } catch (const nlohmann::json::exception &e) {
        spdlog::error("Failed to parse carbon intensity from file, received json exception: {}", e.what());
        return std::nullopt;
    } catch (...) {
        spdlog::error("Failed to parse carbon intensity from file." );
        return std::nullopt;
    }
}

std::vector<CarbonIntensity> CarbonIntensity::from_files(const std::filesystem::path &store_path)
{
    if (!std::filesystem::exists(store_path)) {
        return {};
    }

    const auto it = std::filesystem::directory_iterator(store_path);
    std::vector<CarbonIntensity> intensities;
    for (const auto &file : it) {
        const auto intensity = from_file(file.path());
        if (intensity) {
            intensities.push_back(*intensity);
        }
    }
    return intensities;
}

CarbonIntensity CarbonIntensity::interpolate(const CarbonIntensity &prev, const CarbonIntensity &next, const std::time_t time)
{
    assert(prev.zone == next.zone && prev.datetime < next.datetime && time >= prev.datetime && time <= next.datetime);

    // Calculate the progress ratio between prev and next timestamps (0.0 to 1.0)
    const double time_progress = static_cast<double>(time - prev.datetime) / static_cast<double>(next.datetime - prev.datetime);

    // Linearly interpolate the intensity based on the time progress
    const auto interpolated_intensity = prev.intensity + (next.intensity - prev.intensity) * time_progress;

    return CarbonIntensity{
        .zone = prev.zone,
        .intensity = static_cast<int>(interpolated_intensity),
        .datetime = time,
        .updated_at = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()),
        .emission_factor_type = prev.emission_factor_type,
        .is_estimated = true,
        .estimation_method = "INTERPOLATION",
        .is_forecast = next.is_forecast,
        .forecast_method = next.is_forecast ? "INTERPOLATION" : "",
        .is_synthetic = true,
    };
}

bool CarbonIntensity::write_to_file(const std::filesystem::path &path) const
{
    std::ofstream file(path);
    if (!file.is_open()) {
        return false;
    }

    file << nlohmann::json(*this);
    file.close();
    return true;
}

}