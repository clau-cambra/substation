//
// Created by Claudio Cambra on 14/7/24.
//

#include "utils.h"

#if __linux__
#include "linuxprocessthrottle.h"
#elif __APPLE__
#include "macosprocessthrottle.h"
#endif


namespace Substation::CarbonMinimisation::Throttling {
    ProcessThrottle* createProcessThrottle() {
        #if __linux__
        return new LinuxProcessThrottle();
        #elif __APPLE__
        return new MacOSProcessThrottle();
        #endif
    }
};