//
// Created by Claudio Cambra on 14/7/24.
//

#include "taskscheduler.h"

#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>

#include <spdlog/spdlog.h>

#include <substation/common/event.h>
#include <substation/common/eventloop.h>

#include "taskmonitor.h"

namespace Substation::CarbonMinimisation::Execution
{

TaskScheduler *TaskScheduler::get_instance()
{
    static TaskScheduler instance;
    return &instance;
}

TaskScheduler::~TaskScheduler() noexcept
{
    for (auto &thread : m_threads) {
        if (thread.joinable()) {
            thread.join();
        }
    }
}

void TaskScheduler::run_when_minimum_carbon(const std::function<void()> &function,
                                            const std::chrono::seconds run_within,
                                            const std::time_t task_submission_time)
{
    constexpr auto CARBON_INTENSITY_CHECK_INTERVAL = std::chrono::seconds(1);
    const auto now = std::chrono::system_clock::now();
    const auto run_within_time_t = run_within.count();
    const auto min_period_opt = m_carbon_curve->lowest_intensity_period(task_submission_time,
                                                                        task_submission_time + run_within_time_t,
                                                                        run_within_time_t,
                                                                        CarbonModelling::CarbonIntensityCurve::any_use_past_24h);
    auto remaining_delay_duration = std::chrono::system_clock::from_time_t(task_submission_time + run_within_time_t) - now;
    if (min_period_opt.has_value()) {
        const auto &min_period = *min_period_opt;
        const auto min_ci = min_period.intensities[min_period.min_intensity_index];
        const auto distance_to_min_ci_dt = min_ci.datetime - std::chrono::system_clock::to_time_t(now);
        remaining_delay_duration = std::chrono::seconds(distance_to_min_ci_dt);
    }
    if (now + remaining_delay_duration > now) {
        const auto next_check_time = std::chrono::system_clock::now() + CARBON_INTENSITY_CHECK_INTERVAL;
        Common::EventLoop::get_instance()->add_event(
            {next_check_time, std::bind_front(&TaskScheduler::run_when_minimum_carbon, this, function, run_within, task_submission_time)});
    } else {
        function();
    }
}

void TaskScheduler::schedule_task(const std::function<void()> &task, const std::chrono::seconds run_within)
{
    const auto task_submission_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    const auto run_in_thread = [this, task] {
        std::thread thread(task);
        std::scoped_lock<std::mutex> lock(m_threads_mutex);
        m_threads.push_back(std::move(thread));
    };
    Common::EventLoop::get_instance()->execute(std::bind_front(&TaskScheduler::run_when_minimum_carbon, this, run_in_thread, run_within, task_submission_time));
}

void TaskScheduler::schedule_task(const IteratingTaskDescriptor &task_desc)
{
    const auto exec_task = [this, task_desc] {
        assert(this == get_instance());
        if (task_desc.throttle_desc.has_value()) {
            const auto monitor = std::make_shared<TaskMonitor>(task_desc, m_carbon_curve);
            auto lowest_intensity_period_changed_callback = [this, monitor, task_desc](const CarbonModelling::CarbonIntensityCurve::Period &period) {
                // Once we start running the task and get a reasonable duration estimate, we can more accurate schedule for the lowest intensity period.
                // Additionally, iff the lowest intensity period has changed and it is more efficient to reschedule this task, then let's do so.
                // First use the expected time per iteration to check if it is even worth it to reschedule or not.
                const auto new_period_start = std::chrono::system_clock::from_time_t(period.intensities[period.min_intensity_index].datetime);
                if (std::chrono::system_clock::now() + monitor->estimated_time_per_iter() >= new_period_start) {
                    spdlog::info("Time per iteration is greater than the time to the lowest intensity period, not rescheduling task");
                    m_carbon_curve->print_ascii_graph();
                    return;
                }
                spdlog::info("New lowest intensity period: {}, rescheduling task", std::chrono::system_clock::to_time_t(new_period_start));
                monitor->stop();
                // Restart the process with a new task monitor later
                const auto new_end = std::ranges::remove(m_task_monitors, monitor);
                m_task_monitors.erase(new_end.begin(), new_end.end());
                schedule_task(task_desc);
            };
            monitor->set_lowest_intensity_period_callback(lowest_intensity_period_changed_callback);
            monitor->start();
            m_task_monitors.push_back(monitor);
        } else {
            std::thread thread([task_desc] {
                while (task_desc.task_iter().has_value()) {
                    // Just run until completion with no throttling applied
                }
            });
        }
    };

    const auto task_submission_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    Common::EventLoop::get_instance()->execute(
        std::bind_front(&TaskScheduler::run_when_minimum_carbon, this, exec_task, *task_desc.run_within, task_submission_time));
}

void TaskScheduler::set_carbon_monitor(const std::shared_ptr<CarbonModelling::CarbonIntensityMonitor> &carbon_monitor)
{
    m_carbon_monitor = carbon_monitor;
    m_carbon_monitor->start();
}

}
