//
// Created by Claudio Cambra on 14/7/24.
//

#include "linuxprocessthrottle.h"

#include <sys/time.h>
#include <sys/resource.h>
#include <iostream>
#include <unistd.h>

namespace Substation::CarbonMinimisation::Throttling {
    void LinuxProcessThrottle::set_cpu_percent_limit(const pid_t pid, const int percentage) {
        // Set the CPU throttle for a process
    }

    void LinuxProcessThrottle::set_memory_limit(const pid_t pid, const size_t limit) {
        const struct rlimit rl { .rlim_cur = limit, .rlim_max = limit };
        if (prlimit(pid, RLIMIT_AS, &rl, nullptr) != 0) perror("Failed to set memory limit");
    }

    void LinuxProcessThrottle::set_priority(const pid_t pid, const int priority) {
        if (setpriority(PRIO_PROCESS, pid, priority) != 0) perror("Failed to set priority");
    }
}