//
// Created by Claudio Cambra on 27/1/25.
//

#include <functional>
#include <optional>

#include "throttledescriptor.h"

namespace Substation::CarbonMinimisation::Execution {

using IteratingTaskDescriptor = struct IteratingTaskDescriptor {
    std::optional<size_t> expected_iters;
    std::function<std::optional<size_t>()> task_iter;
    std::optional<Throttling::ThrottleDescriptor> throttle_desc;
    std::optional<std::chrono::seconds> run_within;
};

}