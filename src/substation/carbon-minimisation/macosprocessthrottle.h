//
// Created by Claudio Cambra on 14/7/24.
//

#ifndef SUBSTATION_MACOSPROCESSTHROTTLE_H
#define SUBSTATION_MACOSPROCESSTHROTTLE_H

#include "processthrottle.h"

namespace Substation::CarbonMinimisation::Throttling
{
class MacOSProcessThrottle : public ProcessThrottle
{
public:
    void set_cpu_percent_limit(pid_t pid, int percentage) override;
    void set_memory_limit(pid_t pid, size_t limit) override;
    void set_priority(pid_t pid, int priority) override;
};
} // Substation::Throttling

#endif // SUBSTATION_MACOSPROCESSTHROTTLE_H
