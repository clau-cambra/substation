//
// Created by Claudio Cambra on 14/7/24.
//

#ifndef SUBSTATION_TASKSCHEDULER_H
#define SUBSTATION_TASKSCHEDULER_H

#include <functional>
#include <mutex>
#include <thread>

#include <substation/carbon-modelling/carbonintensitycurve.h>
#include <substation/carbon-modelling/carbonintensitymonitor.h>

namespace Substation
{

namespace CarbonMinimisation::Execution
{

struct IteratingTaskDescriptor;
class TaskMonitor;

// This class is in charge of running tasks that have been scheduled for later execution. Given a
// completion time-limit, it will schedule the task for execution at an optimal time in terms of
// carbon intensity while ensuring the task is completed on time.
//
// It can also intelligently throttle a given task, given a certain limit descriptor. By using info
// about carbon intensity it can intensify the throttling of an application when carbon intensity is
// higher while ensuring that execution is still completed by the given limit time.

class TaskScheduler : public std::enable_shared_from_this<TaskScheduler>
{
public:
    static TaskScheduler *get_instance();
    ~TaskScheduler() noexcept;

    void schedule_task(const std::function<void()> &task, std::chrono::seconds run_within);
    void schedule_task(const IteratingTaskDescriptor &task_desc);

    std::shared_ptr<CarbonModelling::CarbonIntensityMonitor> get_carbon_monitor() const
    {
        return m_carbon_monitor;
    }
    void set_carbon_monitor(const std::shared_ptr<CarbonModelling::CarbonIntensityMonitor> &carbon_monitor);

    std::shared_ptr<CarbonModelling::CarbonIntensityCurve> get_carbon_curve() const
    {
        return m_carbon_curve;
    }

private:
    void run_when_minimum_carbon(const std::function<void()> &function,
                                 const std::chrono::seconds run_within,
                                 const std::time_t task_submission_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));

    std::mutex m_threads_mutex;
    std::vector<std::thread> m_threads;
    std::vector<std::shared_ptr<TaskMonitor>> m_task_monitors;
    std::shared_ptr<CarbonModelling::CarbonIntensityMonitor> m_carbon_monitor;
    std::shared_ptr<CarbonModelling::CarbonIntensityCurve> m_carbon_curve = std::make_shared<CarbonModelling::CarbonIntensityCurve>();
};
}
}

#endif // SUBSTATION_TASKSCHEDULER_H
