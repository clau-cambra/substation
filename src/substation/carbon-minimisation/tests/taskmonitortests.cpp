//
// Created by Claudio Cambra on 10/2/25.
//

#include <catch2/catch_all.hpp>

#include <chrono>
#include <pthread.h>
#include <semaphore>
#include <vector>

#include <spdlog/spdlog.h>

#include <substation/carbon-minimisation/taskmonitor.h>
#include <substation/carbon-minimisation/throttledescriptor.h>
#include <substation/carbon-minimisation/utils.h>
#include <substation/carbon-modelling/carbonintensity.h>
#include <substation/carbon-modelling/carbonintensitycurve.h>

using namespace Substation::CarbonMinimisation;
using namespace Substation::CarbonModelling;

TEST_CASE("TaskMonitor basic throttling", "[tm-basic]")
{
    auto completed_correctly = false;
    auto semaphore = std::binary_semaphore(0);
    auto result = 1.0;
    auto i = 0;

    pthread_t thread_id = nullptr;
    int64_t last_cpu_time = 0;
    std::chrono::time_point<std::chrono::steady_clock> last_wall_time = std::chrono::steady_clock::now();
    std::vector<double> cpu_percentages;

    const auto iteration =
        [&result, &i, &semaphore, &completed_correctly, &thread_id, &last_cpu_time, &last_wall_time, &cpu_percentages] -> std::optional<size_t> {
        if (thread_id == nullptr) {
            thread_id = ::pthread_self();
        }

        result = std::sqrt(result * result + 1.0);
        if (result > 10000.0) {
            result = 1.0;
        }
        i++;

        const auto current_cpu_time = Execution::get_thread_cpu_time(thread_id);
        const auto current_wall_time = std::chrono::steady_clock::now();

        if (last_cpu_time != 0) {
            const auto cpu_time_delta = current_cpu_time - last_cpu_time;
            const auto wall_time_delta = std::chrono::duration_cast<std::chrono::nanoseconds>(current_wall_time - last_wall_time).count();

            // Calculate percentage
            const auto cpu_usage = 100.0 * static_cast<double>(cpu_time_delta) / static_cast<double>(wall_time_delta);
            cpu_percentages.push_back(cpu_usage);
        }

        last_cpu_time = current_cpu_time;
        last_wall_time = current_wall_time;

        if (i == 10000) {
            completed_correctly = true;
            semaphore.release();
            return std::nullopt;
        }
        return std::make_optional(i);
    };

    const auto throttle_desc = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = 50};
    const auto task_desc = Execution::IteratingTaskDescriptor{
        .task_iter = iteration,
        .throttle_desc = throttle_desc,
    };

    Execution::TaskMonitor monitor(task_desc);
    monitor.start();

    semaphore.acquire();
    REQUIRE(completed_correctly);

    // Calculate average CPU usage
    const double avg_cpu_usage = std::accumulate(cpu_percentages.begin(), cpu_percentages.end(), 0.0) / static_cast<double>(cpu_percentages.size());
    REQUIRE(avg_cpu_usage < throttle_desc.max_cpu_limit_percent);
}

TEST_CASE("TaskMonitor carbon-aware throttling", "[tm-carbon-aware]")
{
    constexpr auto ITERATIONS = 50;
    constexpr auto INTERNAL_ITERATIONS = 5000000;
    constexpr auto STARTING_INTENSITY_SAMPLES = 1000;
    constexpr auto INTENSITY_SAMPLE_SEC_SPREAD = 1;
    constexpr auto MIN_CARBON_INTENSITY = 100;
    constexpr auto MAX_CPU_LIMIT = 50;
    constexpr auto MIN_CPU_LIMIT = 10;
    constexpr auto EXPECTED_ACTUAL_MAX_POS_DIFF = 3;
    constexpr auto EXPECTED_ACTUAL_MAX_NEG_DIFF = -3;

    const auto carbon_curve = std::make_shared<Substation::CarbonModelling::CarbonIntensityCurve>();
    // Create a series of carbon intensities over time
    const auto now = std::chrono::system_clock::now();
    auto time = now;
    auto intensity = MIN_CARBON_INTENSITY;
    const auto generate_intensities = [INTENSITY_SAMPLE_SEC_SPREAD, &carbon_curve, &now, &time, &intensity](const std::chrono::seconds &duration) {
        while (time < now + duration + std::chrono::seconds(INTENSITY_SAMPLE_SEC_SPREAD)) {
            carbon_curve->add_intensity(CarbonIntensity{.zone = "TEST",
                                                        .intensity = intensity,
                                                        .datetime = std::chrono::system_clock::to_time_t(time),
                                                        .updated_at = std::chrono::system_clock::to_time_t(now),
                                                        .is_forecast = true,
                                                        .forecast_method = "TEST"});
            time += std::chrono::seconds(1);
            intensity += 25;
        }
    };
    generate_intensities(std::chrono::seconds(STARTING_INTENSITY_SAMPLES));

    auto completed_correctly = false;
    auto semaphore = std::binary_semaphore(0);
    auto result = 1.0;
    auto i = 0;
    auto max_intensity = intensity;

    pthread_t thread_id = nullptr;
    int64_t last_cpu_time = 0;
    std::chrono::time_point<std::chrono::steady_clock> last_wall_time = std::chrono::steady_clock::now();
    std::vector<std::pair<double, double>> cpu_percentages;

    const auto iteration =
        [&result, &i, &semaphore, &completed_correctly, &thread_id, &last_cpu_time, &last_wall_time, &cpu_percentages, &carbon_curve, &max_intensity]
        -> std::optional<size_t> {
        if (thread_id == nullptr) {
            thread_id = ::pthread_self();
        }

        for (auto j = 0; j < INTERNAL_ITERATIONS; ++j) {
            result = std::sqrt(result * result + 1.0);
            if (result > 10000.0) {
                result = 1.0;
            }
        }
        i++;

        const auto current_cpu_time = Execution::get_thread_cpu_time(thread_id);
        const auto current_wall_time = std::chrono::steady_clock::now();
        auto cpu_usage = 0.0;

        if (last_cpu_time != 0) {
            const auto cpu_time_delta = current_cpu_time - last_cpu_time;
            const auto wall_time_delta = std::chrono::duration_cast<std::chrono::nanoseconds>(current_wall_time - last_wall_time).count();
            cpu_usage = 100.0 * static_cast<double>(cpu_time_delta) / static_cast<double>(wall_time_delta);
        }

        last_cpu_time = current_cpu_time;
        last_wall_time = current_wall_time;

        // Check if the cpu usage percent was proportional
        const auto current_intensity = carbon_curve->intensity_at(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
        REQUIRE(current_intensity.has_value());

        const auto current_intensity_ratio = (max_intensity - current_intensity->intensity) / static_cast<double>(max_intensity - MIN_CARBON_INTENSITY);
        const auto expected_cpu_usage = MIN_CPU_LIMIT + (MAX_CPU_LIMIT - MIN_CPU_LIMIT) * current_intensity_ratio;

        if (cpu_usage != 0.0) {
            spdlog::info("Expected: {} | Actual: {}", expected_cpu_usage, cpu_usage);
            cpu_percentages.push_back(std::make_pair(expected_cpu_usage, cpu_usage));
        }

        if (i == ITERATIONS) {
            completed_correctly = true;
            semaphore.release();
            return std::nullopt;
        }
        return std::make_optional(i);
    };

    const auto throttle_desc = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = MAX_CPU_LIMIT, .min_cpu_limit_percent = MIN_CPU_LIMIT};
    const auto task_desc = Execution::IteratingTaskDescriptor{.expected_iters = ITERATIONS, .task_iter = iteration, .throttle_desc = throttle_desc};
    Execution::TaskMonitor monitor(task_desc,
                                   carbon_curve,
                                   [&generate_intensities, &max_intensity, &carbon_curve, &now](const std::chrono::seconds total_duration) {
                                       generate_intensities(total_duration);
                                       max_intensity = carbon_curve->intensity_at(std::chrono::system_clock::to_time_t(now + total_duration))->intensity;
                                   });
    monitor.start();

    semaphore.acquire();
    REQUIRE(completed_correctly);

    // Calculate average CPU usage
    const double avg_cpu_usage_diff = std::accumulate(cpu_percentages.begin(),
                                                      cpu_percentages.end(),
                                                      0.0,
                                                      [](const double accumulating_value, const std::pair<double, double> &pair) {
                                                          return accumulating_value + (pair.first - pair.second);
                                                      })
        / static_cast<double>(cpu_percentages.size());
    spdlog::info("Avg CPU usage diff: {}", avg_cpu_usage_diff);
    REQUIRE(avg_cpu_usage_diff < EXPECTED_ACTUAL_MAX_POS_DIFF);
    REQUIRE(avg_cpu_usage_diff > EXPECTED_ACTUAL_MAX_NEG_DIFF);
}

TEST_CASE("TaskMonitor handles invalid throttle descriptors", "[tm-invalid]")
{
    auto completed_correctly = false;
    auto semaphore = std::binary_semaphore(0);

    const auto iteration = [&semaphore, &completed_correctly]() -> std::optional<size_t> {
        completed_correctly = true;
        semaphore.release();
        return std::nullopt;
    };

    // Test with invalid CPU limits
    SECTION("CPU limit > 100%")
    {
        const auto throttle_desc = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = 150};
        const auto task_desc = Execution::IteratingTaskDescriptor{
            .task_iter = iteration,
            .throttle_desc = throttle_desc,
        };

        Execution::TaskMonitor monitor(task_desc);
        monitor.start();

        semaphore.acquire();
        REQUIRE(completed_correctly);
    }

    SECTION("CPU limit = 0%")
    {
        const auto throttle_desc = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = 0};
        const auto task_desc = Execution::IteratingTaskDescriptor{
            .task_iter = iteration,
            .throttle_desc = throttle_desc,
        };

        Execution::TaskMonitor monitor(task_desc);
        monitor.start();

        semaphore.acquire();
        REQUIRE(completed_correctly);
    }

    SECTION("Min CPU limit > Max CPU limit")
    {
        const auto throttle_desc = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = 50, .min_cpu_limit_percent = 75};
        const auto task_desc = Execution::IteratingTaskDescriptor{
            .task_iter = iteration,
            .throttle_desc = throttle_desc,
        };

        Execution::TaskMonitor monitor(task_desc);
        monitor.start();

        semaphore.acquire();
        REQUIRE(completed_correctly);
    }
}
