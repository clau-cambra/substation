//
// Created by Claudio Cambra on 27/12/24.
//

#include <catch2/catch_all.hpp>

#include <chrono>
#include <numeric>
#include <pthread.h>
#include <semaphore>
#include <vector>

#include <spdlog/spdlog.h>

#include <substation/carbon-minimisation/taskmonitor.h>
#include <substation/carbon-minimisation/taskscheduler.h>
#include <substation/carbon-minimisation/throttledescriptor.h>
#include <substation/carbon-modelling/carbonintensity.h>

using namespace Substation::CarbonMinimisation;
using namespace Substation::CarbonModelling;

TEST_CASE("Basic task scheduler task scheduling", "[tr-basic]")
{
    const auto now = std::chrono::system_clock::now();
    const auto forecast_dt = now + std::chrono::seconds(1);
    const auto ci_forecast = CarbonIntensity{.zone = "TEST",
                                             .intensity = 100,
                                             .datetime = forecast_dt.time_since_epoch().count(),
                                             .updated_at = now.time_since_epoch().count(),
                                             .is_forecast = true,
                                             .forecast_method = "TEST"};

    const auto task_scheduler = Execution::TaskScheduler::get_instance();
    const auto within = std::chrono::seconds(5);
    const auto max_deadline = std::chrono::system_clock::now() + within + std::chrono::seconds(1);
    auto completed_correctly = false;
    auto semaphore = std::binary_semaphore(0);

    task_scheduler->schedule_task(
        [&semaphore, max_deadline, &completed_correctly] {
            REQUIRE(std::chrono::system_clock::now() < max_deadline);
            completed_correctly = true;
            semaphore.release();
        },
        std::chrono::duration_cast<std::chrono::minutes>(within));
    semaphore.acquire();
    REQUIRE(completed_correctly);
}

TEST_CASE("Task scheduler task scheduling, with throttling applied", "[tr-throttling]")
{
    const auto task_scheduler = Execution::TaskScheduler::get_instance();
    const auto within = std::chrono::seconds(5);
    const auto exec_start_deadline = std::chrono::system_clock::now() + within  + std::chrono::seconds(1);
    const auto throttle_descriptor = Throttling::ThrottleDescriptor{.max_cpu_limit_percent = 50};

    auto completed_correctly = false;
    auto semaphore = std::binary_semaphore(0);
    auto result = 1.0;
    auto i = 0;

    pthread_t thread_id = nullptr;
    int64_t last_cpu_time = 0;
    std::chrono::time_point<std::chrono::steady_clock> last_wall_time = std::chrono::steady_clock::now();
    std::vector<double> cpu_percentages;

    const auto iteration = [&result, &i, &semaphore, &completed_correctly, &thread_id, &last_cpu_time, &last_wall_time, &cpu_percentages, &exec_start_deadline]
        -> std::optional<size_t> {
        if (thread_id == nullptr) {
            // This means the task has now been scheduled for execution. Ensure this has happened within expected time.
            REQUIRE(std::chrono::system_clock::now() < exec_start_deadline);
            thread_id = ::pthread_self();
        }

        result = std::sqrt(result * result + 1.0);
        if (result > 10000.0) {
            result = 1.0;
        }
        i++;

        const auto current_cpu_time = Execution::get_thread_cpu_time(thread_id);
        const auto current_wall_time = std::chrono::steady_clock::now();

        if (last_cpu_time != 0) {
            const auto cpu_time_delta = current_cpu_time - last_cpu_time;
            const auto wall_time_delta = std::chrono::duration_cast<std::chrono::nanoseconds>(current_wall_time - last_wall_time).count();

            // Calculate percentage
            const auto cpu_usage = 100.0 * static_cast<double>(cpu_time_delta) / static_cast<double>(wall_time_delta);
            cpu_percentages.push_back(cpu_usage);
        }

        last_cpu_time = current_cpu_time;
        last_wall_time = current_wall_time;

        if (i == 10000) {
            completed_correctly = true;
            semaphore.release();
            return std::nullopt;
        }
        return std::make_optional(i);
    };

    const auto task_desc = Execution::IteratingTaskDescriptor{.task_iter = iteration,
                                                              .throttle_desc = throttle_descriptor,
                                                              .run_within = within};
    task_scheduler->schedule_task(task_desc);
    semaphore.acquire();
    REQUIRE(completed_correctly);

    // Calculate average CPU usage
    const double avg_cpu_usage = std::accumulate(cpu_percentages.begin(), cpu_percentages.end(), 0.0) / static_cast<double>(cpu_percentages.size());
    spdlog::info("Average cpu usage \% during test was: {}", avg_cpu_usage);
    REQUIRE(avg_cpu_usage < throttle_descriptor.max_cpu_limit_percent);
}

TEST_CASE("Task scheduler reschedules iterating task when lowest intensity period changes", "[reschedule]")
{
    const auto scheduler = Execution::TaskScheduler::get_instance();
    const auto curve = scheduler->get_carbon_curve();

    // Seed the curve with a high intensity reading far in the future.
    const auto now = std::chrono::system_clock::now();

    for (auto i = -3000; i < 3000; ++i) {
        const auto time = std::chrono::system_clock::to_time_t(now + std::chrono::seconds(i) - std::chrono::hours(24));
        curve->add_intensity(CarbonIntensity{.zone = "TEST",
                                             .intensity = 100,
                                             .datetime = time,
                                             .updated_at = time,
                                             .is_forecast = false,
                                             .forecast_method = "TEST"});
    }

    bool rescheduled = false;
    std::binary_semaphore semaphore(0);

    // Create an iterating task that returns a value a few times, then stops.
    // When it stops (returns nullopt), we consider it has run its (rescheduled) version.
    int iterations = 0;
    auto last_iter_time = std::chrono::system_clock::now();
    
    auto iter_task = [&iterations, &semaphore, &rescheduled, &last_iter_time]() -> std::optional<size_t> {
        ++iterations;
        if (iterations >= 150) {
            semaphore.release();
            return std::nullopt;
        }
        const auto diff = std::chrono::system_clock::now() - last_iter_time;
        const auto expected_diff = std::chrono::milliseconds(100);
        spdlog::info("Task iteration time diff: {} ms", std::chrono::duration_cast<std::chrono::milliseconds>(diff).count());
        if (diff > expected_diff) {
            rescheduled = true;
            semaphore.release();
            return std::nullopt;
        }
        last_iter_time = std::chrono::system_clock::now();
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        return iterations;
    };

    // Create a throttling descriptor so that the scheduler uses a TaskMonitor.
    Throttling::ThrottleDescriptor throttle_desc{.max_cpu_limit_percent = 50};
    Execution::IteratingTaskDescriptor task_desc{.expected_iters = 150,
                                                 .task_iter = iter_task,
                                                 .throttle_desc = throttle_desc,
                                                 .run_within = std::chrono::seconds(10)};
    scheduler->schedule_task(task_desc);
    std::this_thread::sleep_for(std::chrono::seconds(1));

    // Now simulate a change in the intensity curve that should force a reschedule.
    for (auto i = -3; i < 3; ++i) {
        const auto time = std::chrono::system_clock::to_time_t(now + std::chrono::seconds(i));
        curve->add_intensity(CarbonIntensity{.zone = "TEST",
                                             .intensity = 100,
                                             .datetime = time,
                                             .updated_at = time,
                                             .is_forecast = false,
                                             .forecast_method = "TEST"});
    }
    for (auto i = 3; i < 100; ++i) {
        const auto time = std::chrono::system_clock::to_time_t(now + std::chrono::seconds(i));
        curve->add_intensity(CarbonIntensity{.zone = "TEST",
                                             .intensity = 50,
                                             .datetime = time,
                                             .updated_at = time,
                                             .is_forecast = false,
                                             .forecast_method = "TEST"});
    }

    semaphore.acquire();
    REQUIRE(rescheduled);
}