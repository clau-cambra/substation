//
// Created by Claudio Cambra on 14/7/24.
//

#ifndef PROCESS_THROTTLE_H
#define PROCESS_THROTTLE_H

#include <string>

namespace Substation::CarbonMinimisation::Throttling
{
class ProcessThrottle
{
public:
    virtual ~ProcessThrottle() = default;

    virtual void set_cpu_percent_limit(pid_t pid, int percentage) = 0;
    virtual void set_memory_limit(pid_t pid, size_t limit) = 0;
    virtual void set_priority(pid_t pid, int priority) = 0;
};
} // Substation

#endif // PROCESS_THROTTLE_H
