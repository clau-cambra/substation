//
// Created by Claudio Cambra on 15/2/25.
//

#include <chrono>
#include <functional>

namespace Substation::Common
{

struct Event {
    std::chrono::system_clock::time_point timeout = std::chrono::system_clock::now();
    std::function<void()> function;

    std::strong_ordering operator<=>(const Event &other) const
    {
        return timeout <=> other.timeout;
    }
};

}