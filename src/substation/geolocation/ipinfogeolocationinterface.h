//
// Created by Claudio Cambra on 25/12/24. Merry Christmas!
//

#pragma once

#include "abstractgeolocationinterface.h"

namespace Substation::Geolocation
{

class IpInfoGeolocationInterface : public AbstractGeolocationInterface
{
public:
    explicit IpInfoGeolocationInterface(const std::string &m_ip_info_token);
    ~IpInfoGeolocationInterface() override = default;

    std::future<std::optional<Geolocation>> get_geolocation() const override;

private:
    std::string m_ip_info_token;
};

}