//
// Created by Claudio Cambra on 28/12/24.
//

#include <catch2/catch_all.hpp>

#include <substation/geolocation/ipinfogeolocationinterface.h>

using namespace Substation::Geolocation;

TEST_CASE("Test getting geolocation via Ip Info API", "ipinfo-feolocation")
{
    const auto env_token = ::getenv("IP_INFO_TOKEN");
    REQUIRE(env_token != nullptr);
    const auto ip_info_token = std::string(env_token);
    const auto geolocation_interface = IpInfoGeolocationInterface(ip_info_token);
    const auto geolocation = geolocation_interface.get_geolocation().get();
    REQUIRE(geolocation != std::nullopt);
}