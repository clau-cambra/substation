//
// Created by Claudio Cambra on 24/2/25.
//

#include "carbonintensitymodel.h"

#include <substation/carbon-modelling/abstractcarbonintensitystorage.h>
#include <substation/carbon-modelling/carbonintensitycurve.h>

#include "carbonintensity.h"

namespace Substation::Qt
{

CarbonIntensityModel::CarbonIntensityModel(const std::shared_ptr<Substation::CarbonModelling::AbstractCarbonIntensityStorage> &storage,
                                           const size_t ingestionSizeLimit,
                                           QObject *const parent)
    : QAbstractTableModel{parent}
    , m_storage{storage}
    , m_curve{std::make_shared<Substation::CarbonModelling::CarbonIntensityCurve>()}
{
    Q_ASSERT(m_curve);
    if (m_storage) {
        if (auto intensitiesInStorage = m_storage->data()) {
            std::ranges::sort(*intensitiesInStorage, std::less{}, &Substation::CarbonModelling::CarbonIntensity::datetime);
            const auto ingestionLimit = std::min(ingestionSizeLimit, intensitiesInStorage->size());
            const auto intensitiesToIngest = std::span(intensitiesInStorage->end() - ingestionLimit, intensitiesInStorage->end());
            for (const auto &ssCi : intensitiesToIngest) {
                m_curve->add_intensity(ssCi);
            }
        }

        m_storage->register_store_callback([this](const Substation::CarbonModelling::CarbonIntensity &intensity) {
            beginInsertRows({}, rowCount(), rowCount());
            m_curve->add_intensity(intensity);
            endInsertRows();
        });
    }
}

int CarbonIntensityModel::rowCount(const QModelIndex &parent) const
{
    Q_ASSERT(m_curve);
    if (parent.isValid()) {
        return 0;
    }
    return static_cast<int>(m_curve->intensity_count());
}

int CarbonIntensityModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return ColumnCount;
}

QVariant CarbonIntensityModel::data(const QModelIndex &index, const int role) const
{
    Q_ASSERT(m_curve);
    if (!checkIndex(index)) {
        return {};
    }
    const auto ssCi = (*m_curve)[index.row()];
    const auto carbonIntensity = Substation::Qt::CarbonIntensity(ssCi);

    switch (index.column()) {
    case DateTimeColumn:
        return carbonIntensity.dateTime();
    case IntensityColumn:
        return carbonIntensity.intensity();
    default:
        break;
    }

    switch (role) {
    case ::Qt::DisplayRole:
        return tr("Carbon intensity of %n gCO2/kWh at ", nullptr, carbonIntensity.intensity()) + carbonIntensity.dateTime().toString();
    case IntensityRole:
        return carbonIntensity.intensity();
    case ZoneRole:
        return carbonIntensity.zone();
    case DateTimeRole:
        return carbonIntensity.dateTime();
    case UpdatedAtRole:
        return carbonIntensity.updatedAt();
    case EmissionFactorTypeRole:
        return carbonIntensity.emissionFactorType();
    case IsEstimatedRole:
        return carbonIntensity.isEstimated();
    case EstimationMethodRole:
        return carbonIntensity.estimationMethod();
    case IsForecastRole:
        return carbonIntensity.isForecast();
    case ForecastMethodRole:
        return carbonIntensity.forecastMethod();
    case IsSyntheticRole:
        return carbonIntensity.isSynthetic();
    default:
        return {};
    }
}

std::shared_ptr<Substation::CarbonModelling::CarbonIntensityCurve> CarbonIntensityModel::curve() const
{
    return m_curve;
}

}
