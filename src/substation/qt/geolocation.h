//
// Created by Claudio Cambra on 24/2/25.
//

#ifndef QT_GEOLOCATION_H
#define QT_GEOLOCATION_H

#include <QObject>

#include <substation/geolocation/geolocation.h>

namespace Substation::Qt
{

class Geolocation
{
    Q_GADGET

    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(float latitude READ latitude CONSTANT)
    Q_PROPERTY(float longitude READ longitude CONSTANT)
    Q_PROPERTY(QString region READ region CONSTANT)
    Q_PROPERTY(QString country READ country CONSTANT)
    Q_PROPERTY(QString timezone READ timezone CONSTANT)

public:
    explicit Geolocation(const Substation::Geolocation::Geolocation &geolocation);

    QString name() const;
    float latitude() const;
    float longitude() const;
    QString region() const;
    QString country() const;
    QString timezone() const;
    Substation::Geolocation::Geolocation substationGeolocation() const;

private:
    Substation::Geolocation::Geolocation m_ssGl;
};

}

Q_DECLARE_METATYPE(Substation::Qt::Geolocation)

#endif // GEOLOCATION_H
