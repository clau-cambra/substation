//
// Created by Claudio Cambra on 24/2/25.
//

#include "geolocation.h"

namespace Substation::Qt
{

Geolocation::Geolocation(const Substation::Geolocation::Geolocation &geolocation)
    : m_ssGl(geolocation)
{
}

QString Geolocation::name() const
{
    return QString::fromStdString(m_ssGl.name);
}

float Geolocation::latitude() const
{
    return m_ssGl.latitude;
}

float Geolocation::longitude() const
{
    return m_ssGl.longitude;
}

QString Geolocation::region() const
{
    return QString::fromStdString(m_ssGl.region);
}

QString Geolocation::country() const
{
    return QString::fromStdString(m_ssGl.country);
}

QString Geolocation::timezone() const
{
    return QString::fromStdString(m_ssGl.timezone);
}

Substation::Geolocation::Geolocation Geolocation::substationGeolocation() const
{
    return m_ssGl;
}

}
