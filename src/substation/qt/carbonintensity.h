//
// Created by Claudio Cambra on 24/2/25.
//

#ifndef QT_CARBONINTENSITY_H
#define QT_CARBONINTENSITY_H

#include <QDateTime>
#include <QString>

#include <substation/carbon-modelling/carbonintensity.h>

namespace Substation::Qt
{

class CarbonIntensity
{
    Q_GADGET

    Q_PROPERTY(QString zone READ zone CONSTANT)
    Q_PROPERTY(int intensity READ intensity CONSTANT)
    Q_PROPERTY(QDateTime dateTime READ dateTime CONSTANT)
    Q_PROPERTY(QDateTime updatedAt READ updatedAt CONSTANT)
    Q_PROPERTY(QString emissionFactorType READ emissionFactorType CONSTANT)
    Q_PROPERTY(bool isEstimated READ isEstimated CONSTANT)
    Q_PROPERTY(QString estimationMethod READ estimationMethod CONSTANT)
    Q_PROPERTY(bool isForecast READ isForecast CONSTANT)
    Q_PROPERTY(QString forecastMethod READ forecastMethod CONSTANT)
    Q_PROPERTY(bool isSynthetic READ isSynthetic CONSTANT)

public:
    explicit CarbonIntensity(const Substation::CarbonModelling::CarbonIntensity &ssCi);

    QString zone() const;
    int intensity() const;
    QDateTime dateTime() const;
    QDateTime updatedAt() const;
    QString emissionFactorType() const;
    bool isEstimated() const;
    QString estimationMethod() const;
    bool isForecast() const;
    QString forecastMethod() const;
    bool isSynthetic() const;
    Substation::CarbonModelling::CarbonIntensity substationCarbonIntensity() const;
    bool operator== (const CarbonIntensity &other) const;

private:
    Substation::CarbonModelling::CarbonIntensity m_ssCi;
};

}

Q_DECLARE_METATYPE(Substation::Qt::CarbonIntensity)

#endif